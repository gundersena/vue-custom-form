export const psychForm = {

  name: "Psycological Safety",

  steps: [{
    prompt: "If you make a mistake on this team, it's held against you.",
    fields: [{
      type: "range",
      value: null, /* Where we capture the data */
    }],
  },
  {
    prompt: "It is safe to take a risk on this team.",
    fields: [{
      type: "range",
      value: null,
    }]
  },
  {
    prompt: "Working with members on this team, my talents are valued.",
    fields: [{
      type: "range",
      value: null,
    }]
  }],
  
}

/* Dummy algo for demo purposes, simply takes average of fields. */
export const psychAlgo = (form, res) => {

    const vals = [];

    form.steps.forEach(s => {

      s.fields.forEach(f => {

        vals.push(f.value);

      });

    });

    res.value = Math.floor(vals.reduce((a, b) => a + b) / vals.length);
}

/*

You can edit the form data here without touching app logic.

*/
