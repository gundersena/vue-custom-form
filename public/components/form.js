import Step from "./step.js";

const Form = 
{
	props: ["name", "steps", "submit"],

	components: {
		Step
	},

	setup(props)
	{
		const curr = Vue.ref(0);

		function next() {

			console.log(props.steps);

			if (curr.value > props.steps.length - 2) {
				props.submit();
				return;
			}

			curr.value += 1;

		}

		return { curr, next };
	},

	template: `
		<div class="form">

			<Step v-bind="steps[curr]" />

			<button 
				class="next-button form-button button"
				@click="next"
			>
				Next
			</button>

		</div>
	`
}

export default Form;

/*

Each form consists of multiple steps.

Each step can consist of multiple fields.

Press the next button to go to next step.

*/