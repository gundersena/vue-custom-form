const Range = 
{
	props: ["field"],

	components: {

	},

	setup(props)
	{
		const ref = Vue.ref(50);

		Vue.watch(ref, (c, _p) => {

			props.field.value = parseInt(c);

		});

		return { ref };
	},

	template: `
		<div class="range">

			<div>{{ ref }}</div>

			<input 
				v-model="ref"
				type="range"
				min="0"
				max="100"
				value="50"
				step="1"
			>

		</div>
	`
}

export default Range

/*

Update the results live with the sliding action.

*/