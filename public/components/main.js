import { psychForm, psychAlgo } from "../assets/forms.js";

import Form from "./form.js";
import Range from "./range.js";

const Main = 
{
	components: {
		Form
	},

	setup()
	{
		const res = Vue.ref(0);
		const state = Vue.ref(0);

		function showForm() {
			state.value = 1;
		}

		function submit() {
			psychAlgo(psychForm, res); /* pass by ref flex ;) */
			state.value = 2;
		}

		return { psychForm, state, showForm, submit, res };
	},

	template: `
		<div id="main">

			<div v-if="state == 0">

				<button class="next-button button" @click="showForm">

					Start

				</button>

			</div>

			<Form v-else-if="state == 1" v-bind="psychForm" :submit="submit" />

			<div class="result" v-else>{{ res }}</div>

		</div>
	`
}

export default Main;

/*

App is split into three parts:

1. Choose a form (simplified to a start button for now).

2. Fill out the form (Form component).

3. See the results (will be separate component in future).

The psychForm (or any form) has the double function of being a template as 
well as storing the form data itself.

*/