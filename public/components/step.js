import Range from "./range.js";

const Step =
{
	props: ["prompt", "fields"],

	components: {
		Range
	},

	setup(props)
	{
		return { };
	},

	template: `
		<div class="step">

			<div class="prompt">{{ prompt }}</div>

			<Range v-for="field in fields" :field=field />

		</div>
	`
}

export default Step;

/*

Show the prompt.

Render the appropriate fields, obviously only range field for now.

*/