### Psycology Form Project

Strategy
 - Create requirements.
 - Design UI in Adobe XD.
 - Code.

Design
 - Mobile first.
 - Modern, functional.
 - Take a look at design.png.

Engineering Approach
 - Minimalist tech stack to show proficiency in the fundementals.
 - Generalized, reactive design (form data is stored in forms.js).
 - You can even add multiple fields for each step.

Other cool stuff
 - Custom fonts!
 - Ability to add multiple fields to each step.
 - Scales to different types of forms.
 - You can provide a custom algorithim to process the form data.
 - In fact, you can even have multiple forms.
 - Hosted on my website (public folder).

Bug Tracker
 - Input range doesn't reset position on next().
 - You need to move input at least once to set a value.

I believe this is a very sustainable design/build with little tech debt, 
making the process of adding new features much easier.

Ok, time to sleep...
